//
//  News.swift
//  NewsProject
//
//  Created by Bohdan Hodovanets on 1/15/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import Foundation

struct News: Decodable {
    let articles: [Article]?
    
}

struct Article: Codable {
    let source: Source?
    let author: String?
    let title, description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String?
    let content: String?
    
}


struct Source: Codable {
    let id: String?
    let name: String?
}
