//
//  NewsCell.swift
//  NewsProject
//
//  Created by Bohdan Hodovanets on 1/19/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    @IBOutlet var imgView: UIImageView!
    @IBOutlet var sourceLabel: UILabel!
    @IBOutlet var authorLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
