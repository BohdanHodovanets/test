//
//  NewsViewController.swift
//  NewsProject
//
//  Created by Bohdan Hodovanets on 1/15/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import UIKit
import SafariServices

class NewsViewController: UITableViewController, UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        if let searchBar = searchBar.text {
            let searchKey = "q=" + searchBar + "&"
            newsUrl = "https://newsapi.org/v2/everything?" + searchKey + "apiKey=0e81b777fbf24874b347c516bc8f2292"
            downloadResult()
            
        }
        self.searchBar.endEditing(true)
    }
    
    @IBOutlet var searchBar: UISearchBar!
    var resultArray = [Article] ()
    var newsUrl = "https://newsapi.org/v2/top-headlines?country=us&apiKey=0e81b777fbf24874b347c516bc8f2292"
    
    @IBAction func refreshControl(_ sender: Any) {
        downloadResult()
        DispatchQueue.main.async {
            self.refreshControl?.endRefreshing()
            self.tableView.reloadData()
        }
        print("refresh")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadResult()
        searchBar.delegate = self
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell") as? NewsCell else {
            return UITableViewCell()
        }
        if let sources = resultArray[indexPath.row].source?.name {
            cell.sourceLabel.text = "From: " + sources
        } else { cell.sourceLabel.text = "From: N/A" }
        if let authors = resultArray[indexPath.row].author {
            cell.authorLabel.text = "Author: " + authors
        } else { cell.authorLabel.text = "Author: N/A" }
        if let titles = resultArray[indexPath.row].title {
            cell.titleLabel.text = titles
        } else { cell.titleLabel.text = "Title: N/A" }
        if let descriptions = resultArray[indexPath.row].description {
            cell.descriptionLabel.text = descriptions
        } else { cell.descriptionLabel.text = "Description: N/A" }
        if let url = resultArray[indexPath.row].urlToImage {
            if let imageURL = URL(string: url) {
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: imageURL)
                    if let data = data {
                        let imageFromUrl = UIImage(data: data)
                        DispatchQueue.main.async {
                            cell.imgView.image = imageFromUrl
                        }
                    }
                }
            }
        }
        return cell
    }
    
    func downloadResult () {
        let jsonUrlString = URL(string: newsUrl)
        if let url = jsonUrlString {
            URLSession.shared.dataTask(with: url) {
                (data, response, err) in
                guard let data = data else { return }
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let downloadedArticles =  try decoder.decode(News.self, from: data)
                    if let downloadedArticles = downloadedArticles.articles {
                        self.resultArray = downloadedArticles
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                        print(downloadedArticles)
                    }
                } catch let jsonError {
                    print("Error serializing json:", jsonError)
                }
                }.resume()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let program = resultArray[indexPath.row]
        if let url = program.url {
            self.showWeb(url: url)
        }
    }
    
    func showWeb(url: String) {
        let webURL = URL(string: url)!
        let webVC = SFSafariViewController(url: webURL)
        
        present(webVC, animated: true, completion: nil)
    }
    
    func searchBarShouldReturn(_ searchBar: UISearchBar) -> Bool {
        self.view.endEditing(true)
        return false
    }
    

}
